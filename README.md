# This is a fork of the original PainlessMeshListener but reformated to work out of the box with raspberry pi's the other version was not compiling out of the box correctly

# painlessMeshListener Pi Version

This is a simple mesh listener that can be used in conjunction with a [painlessMesh](https://gitlab.com/BlackEdder/painlessMesh) mesh. For more details on how to set up painlessMesh to log to this listener see the usage instructions below and the [wiki](https://gitlab.com/BlackEdder/painlessMesh/wikis/bridge-between-mesh-and-another-network)


## Install

Easiest way to get the listener to run is to clone the git repository and compile it using `dub`. Dub will download the needed dependencies, compile the code and run it. 

```
git clone https://gitlab.com/zombodotcom/painlessMeshListener
cd painlessMeshListener
dub
```

Alternatively, after compilation by dub you can move the resulting binary (`painlessmeshlistener`) to a location of your choosing.

`dub` is part of the D compiler. See below for installation instructions 

## D Compiler installation



### ARM based machine (raspberry-pi)

DMD does not have support for the ARM architecture yet (raspberry-pi). So on an ARM machine you will need to install a version of [LDC](https://github.com/ldc-developers/ldc/releases).  You can install the working version 1.9.0 for the pi with this version as follows:

```
wget https://github.com/ldc-developers/ldc/releases/download/v1.9.0/ldc2-1.9.0-linux-armhf.tar.xz
tar xf ldc2-1.9.0-linux-armhf.tar.xz
sudo mv ldc2-1.9.0-linux-armhf /opt/
sudo apt-get install libevent-dev libssl-dev
sudo apt-get install gcc
```

Add the bin subdirectory to your path, following method will add path temporally, for permanent PATH you need to edit ~/.profile file.

```
export PATH="/opt/ldc2-1.9.0-linux-armhf/bin:$PATH"
```
just edit ~/.profile to incluide the path of the LDC2 compiler. 

Now dub should be properly installed and you can follow the regular installation instructions.

## Usage

`painlessMeshListener` can be run as both a server or a client node. By default it runs as a server node. 

### Server

This is the default mode when you run painlessMeshListener

```
./painlessMeshListener
```

In this mode it will listen on a port (5555 by default, use `-p <port>` to change this). This mode requires you to include a [bridge](https://gitlab.com/BlackEdder/painlessMesh/blob/master/examples/bridge/bridge.ino) node in your network. 

### Client

Running it as a client requires you to setup a WiFi connection to the mesh network. Then you can run `painlessMeshListener` to connect to the node you are connected to. You need the IP address of this node (this is typically the IP of the gateway of the WiFi connection to the mesh).

```
./painlessMeshListener -c <ip>
```

## Changing the code

The listener is currently setup to log all the received messages to the console and to notify the rest of the mesh that they can log to this listener. If you want to change this behaviour then the best places to start are at 

- [log](https://gitlab.com/zombodotcom/painlessMeshListener/blob/master/source/app.d#L49)
- [broadcast](https://gitlab.com/zombodotcom/painlessMeshListener/blob/master/source/app.d#L59)
