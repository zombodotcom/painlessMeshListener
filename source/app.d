#!/usr/bin/env dub
/+ dub.sdl:
	name "myscript"
	dependency "scriptlike" version="~>0.10.2"
	dependency "vibe-d:core" version="~>0.8.0-beta.5"
	dependency "vibe-d:stream" version="~>0.8.0-beta.5"
    dependency "painlessjson" version="~>1.3.6"
    versions "VibeDefaultMain"
+/

/+
  Program implementing a painlessMesh node that can run on any computer
  
    Dependencies:
        D programming language: https://dlang.org

  To run just make this script executable and run it. First time
  it will need to install some dependencies, so it might take some
  time to startup.

  The main functions you are likely to want to edit are the receivedMessage function and the Hello network function.
+/
import scriptlike;
import painlessjson;
import std.json;


import vibe.core.core : runTask, sleep, readOption, runEventLoop, finalizeCommandLineOptions, lowerPrivileges;
import vibe.core.log;
import vibe.core.net : TCPConnection, listenTCP, resolveHost, NetworkAddress;
import vibe.stream.operations : readLine;

import core.time;

import painlessmesh;

size_t port = 5555;

Node node;

void main()
{
    node = new Node();
    node.nodeId = uniform(0, int.max);

    // Feel free to edit the two functions below:
    // This function is called when receiving a message
    node.onReceive((size_t from, JSONValue msg) {
        msg.writeln;
    });

    // This tasks broadcasts a message every two minutes
    runTask(() {
        while(true) {
            JSONValue[string] msg;
            msg["topic"] = JSONValue("logNode");
            msg["nodeId"] = JSONValue(node.nodeId);
            node.sendBroadcast(msg.toJSON.to!string);
            sleep(120.seconds());
        }
    });

    bool verbose = false;
    readOption("verbose|v", &verbose, "Enable debug messages");
    if (verbose)
        setLogLevel(LogLevel.debug_);


    // Rest is needed to run the server/client. Don't change this unless you know what you are doing
    readOption("port|p", &port, "Optional port (default is 5555)");

    import std.conv : to;
    NetworkAddress addr;
    string ip;
    readOption("client|c", &ip, "Run in client mode. Need to provide ip of the node to connect to.");
    if (!ip.empty()) {
        addr = resolveHost(ip);
        import std.conv : to;
        addr.port(port.to!ushort); 
        node.initClient(addr);
    } else {
        node.initServer(port);
    }

	if (!finalizeCommandLineOptions())
		return;

	lowerPrivileges();
	runEventLoop();
}

